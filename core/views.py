import copy
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login

# Create your views here.

def can_include(node, user):
    view_name = node.get(settings.VIEW, None)
    if not view_name:
        view_name = node.get(settings.TITLE)
    childs = node.get(settings.CHILDS, [])
    perms = settings.URL_PERMISSIONS.get(view_name,[])
    return user.has_perms(perms)

def create_node(item):
    node = {
        settings.VIEW:item.get(settings.VIEW),
        settings.TITLE:item.get(settings.TITLE),
        settings.VISIBLE:item.get(settings.VISIBLE, False),
        settings.CHILDS:[],
    }
    return node

def traverse(menu_items, user):
    menu = []
    for item in menu_items:
        if can_include(item, user):
            node = create_node(item)

            for child in item.get(settings.CHILDS, []):
                if can_include(child, user):
                    child_node = create_node(child)
                    node[settings.CHILDS].append(child_node)
            if node[settings.CHILDS]:
                menu.append(node)
    return menu

class BaseSupplyChainViewMixin(object):
    child_of_view = None

    def get_context_data(self, **kwargs):
        context = super(BaseSupplyChainViewMixin,self) \
                        .get_context_data(**kwargs)
        context['navigation'] = self.build_navigation(self.request.user)

        if context.get('paginator', None):
            p = context['paginator']
            page = context['page_obj']
            start = max(page.number-5,0)
            end = min(page.number+4, p.num_pages)
            context['pages'] = [i+1 for i in range(start, end)]
            context['last_page'] = p.num_pages
        return context

    def build_navigation(self, user):
        menu = traverse(settings.NAVIGATION_MENU, self.request.user)
        # import json
        # s = json.dumps(menu, sort_keys=True, indent=4)
        # print '\n'.join([l.rstrip() for l in  s.splitlines()])
        return menu






