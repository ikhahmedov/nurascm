from django.conf.urls import url, include, patterns
from views import (ProductListView, ProductEditView, ProductCreateView,
                   ProductDeleteView, CategoryListView, CategoryEditView,
                   CategoryCreateView, CategoryDeleteView, WarehouseHistoryView,
                   WarehouseDashboardView)


urlpatterns = patterns('',
    url(r'^products/$', ProductListView.as_view(), name='warehouse_products'),
    url(r'^products/(?P<pk>\d+)/$', ProductEditView.as_view(), name='warehouse_product_edit'),
    url(r'^products/new/$', ProductCreateView.as_view(), name='warehouse_product_new'),
    url(r'^products/(?P<pk>\d+)/delete/$', ProductDeleteView.as_view(), name='warehouse_product_delete'),

    url(r'^categories/$', CategoryListView.as_view(), name='warehouse_categories'),
    url(r'^categories/(?P<pk>\d+)/$', CategoryEditView.as_view(), name='warehouse_category_edit'),
    url(r'^categories/new/$', CategoryCreateView.as_view(), name='warehouse_category_new'),
    url(r'^categories/(?P<pk>\d+)/delete/$', CategoryDeleteView.as_view(), name='warehouse_category_delete'),

    url(r'^history/$', WarehouseHistoryView.as_view(), name='warehouse_history'),
    url(r'^dashboard/$', WarehouseDashboardView.as_view(), name='warehouse_dashboard'),
)
