# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=32, verbose_name=b'Nomlanish')),
            ],
            options={
                'permissions': (('read_category', 'Can read category data'),),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=32, verbose_name=b'Nomlanish')),
                ('description', models.TextField(null=True, verbose_name=b"Ma'lumot", blank=True)),
                ('internal', models.BooleanField(default=False, help_text=b'products for internal use only, will not be shown to customer', verbose_name=b'Ichki')),
                ('counts', models.TextField(default=b'', help_text=b'sotiladigan miqdorlar soni')),
                ('category', models.ForeignKey(verbose_name=b'Kategoriya', to='warehouse.Category')),
            ],
            options={
                'permissions': (('read_product', 'Can read product data'),),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Warehouse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32)),
                ('company', models.OneToOneField(to='companies.Company')),
            ],
            options={
                'permissions': (('read_warehouse', 'Can read warehouse data'),),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WarehouseStock',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.FloatField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('product', models.ForeignKey(to='warehouse.Product')),
                ('warehouse', models.ForeignKey(to='warehouse.Warehouse')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WarehouseTransaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.FloatField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('stock_type', models.IntegerField(default=0, help_text=b'added or removed from warehouse', db_index=True, choices=[(0, 'addition'), (1, 'remove')])),
                ('product', models.ForeignKey(to='warehouse.Product')),
                ('warehouse', models.ForeignKey(to='warehouse.Warehouse')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='warehouse',
            name='stocks',
            field=models.ManyToManyField(related_name='stocks', through='warehouse.WarehouseStock', to='warehouse.Product'),
            preserve_default=True,
        ),
    ]
