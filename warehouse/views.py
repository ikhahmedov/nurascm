from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic import (ListView, View, TemplateView,
                                  UpdateView, CreateView, DeleteView)
from django.forms import models as model_forms
from django.contrib import messages
from django.db.utils import IntegrityError
from django.core.exceptions import ValidationError
from core.views import BaseSupplyChainViewMixin
from models import (Product, Category, WarehouseTransaction,
                    STOCK_ADD, STOCK_DEL, WarehouseStock)
from companies.models import get_main_company

# Create your views here.
# Products
class ProductListView(BaseSupplyChainViewMixin, ListView):
    template_name = 'warehouse/products.html'
    model = Product

class ProductEditView(BaseSupplyChainViewMixin, UpdateView):
    template_name = 'warehouse/product_edit.html'
    model = Product
    success_url = reverse_lazy('warehouse_products')

class ProductCreateView(BaseSupplyChainViewMixin, CreateView):
    template_name = 'warehouse/product_edit.html'
    model = Product
    success_url = reverse_lazy('warehouse_products')

    def form_valid(self, form):
        res = super(ProductCreateView, self).form_valid(form)
        warehouse = self.request.user.company.warehouse
        stock = WarehouseStock(product=self.object, warehouse=warehouse,
                                   quantity=0)
        stock.save()
        return res

class ProductDeleteView(BaseSupplyChainViewMixin, DeleteView):
    template_name = 'warehouse/product_delete.html'
    model = Product
    success_url = reverse_lazy('warehouse_products')

# Categories
class CategoryListView(BaseSupplyChainViewMixin, ListView):
    template_name = 'warehouse/categories.html'
    model = Category

class CategoryEditView(BaseSupplyChainViewMixin, UpdateView):
    template_name = 'warehouse/category_edit.html'
    model = Category
    success_url = reverse_lazy('warehouse_categories')

class CategoryCreateView(BaseSupplyChainViewMixin, CreateView):
    template_name = 'warehouse/category_edit.html'
    model = Category
    success_url = reverse_lazy('warehouse_categories')

class CategoryDeleteView(BaseSupplyChainViewMixin, DeleteView):
    template_name = 'warehouse/category_delete.html'
    model = Category
    success_url = reverse_lazy('warehouse_categories')

class WarehouseHistoryView(BaseSupplyChainViewMixin, ListView):
    template_name = 'warehouse/history.html'
    queryset = WarehouseTransaction.objects.all().order_by('-created')
    paginate_by = 20

    def get_queryset(self, *args, **kwargs):
        queryset = super(WarehouseHistoryView, self).get_queryset(*args, **kwargs)
        queryset = queryset.select_related('product')
        q = queryset.filter(warehouse=self.request.user.company.warehouse.id)
        return q

class WarehouseDashboardView(BaseSupplyChainViewMixin, TemplateView):
    template_name = 'warehouse/dashboard.html'
    add_prefix = 'add_'
    del_prefix = 'del_'
    success_url = reverse_lazy('warehouse_dashboard')

    def get_context_data(self, **kwargs):
        context = super(WarehouseDashboardView, self).get_context_data(**kwargs)
        user = self.request.user
        warehouse = user.company.warehouse
        context['products'] = warehouse.state()
        main_company = get_main_company()
        context['is_main'] = user.company == main_company
        return context

    def post(self, *args, **kwargs):
        POST = self.request.POST.copy()
        warehouse = self.request.user.company.warehouse
        # (product_id, quantity)
        apfx_len = len(self.add_prefix)
        rpfx_len = len(self.del_prefix)
        add = []
        remove = []
        internal_products = Product.objects.filter(internal=True).values_list('pk', flat=True)
        for k,v in POST.items():
            if not v:
                continue

            if k.startswith(self.add_prefix):
                add.append( ( int(k[apfx_len:]), int(v) ) )
            elif k.startswith(self.del_prefix):
                remove.append( ( int(k[rpfx_len:]), int(v) ) )

        add = filter(lambda x:x[0] not in internal_products, add)
        remove = filter(lambda x:x[0] not in internal_products, remove)

        for item in add:
            WarehouseTransaction \
                .objects \
                .create(product_id=item[0],
                        warehouse_id=warehouse.id,
                        quantity=item[1],
                        stock_type=STOCK_ADD)

        for item in remove:
            try:
                WarehouseTransaction \
                    .objects \
                    .create(product_id=item[0],
                            warehouse_id=warehouse.id,
                            quantity=item[1],
                            stock_type=STOCK_DEL)
            except IntegrityError as ie:
                messages.error(self.request, ie.message)

        if messages:
            return self.get(self.request, *args, **kwargs)

        return HttpResponseRedirect(self.success_url)






