from django.db import models, transaction
from django.db.utils import IntegrityError
from django.db.models.signals import post_save
from django.dispatch import receiver
from companies.models import Company
from warehouse.constants import *

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=32, verbose_name='Nomlanish', unique=True)

    class Meta:
        permissions = (
                ("read_category", "Can read category data"),
        )

    def __unicode__(self):
        return self.name

class ProductManager(models.Manager):
    def externals(self):
        return self.get_queryset() \
                    .filter(internal=False)

class Product(models.Model):
    name = models.CharField(max_length=32, verbose_name='Nomlanish', unique=True)
    description = models.TextField(blank=True, null=True, verbose_name="Ma'lumot")
    category = models.ForeignKey(Category, verbose_name='Kategoriya')
    internal = models.BooleanField(default=False, verbose_name='Ichki',help_text='products for internal use only, will not be shown to customer')

    objects = ProductManager()

    class Meta:
        permissions = (
                ("read_product", "Can read product data"),
        )

    def __unicode__(self):
        return self.name


class Stock(models.Model):
    product = models.ForeignKey('Product')
    warehouse = models.ForeignKey('Warehouse')
    quantity = models.FloatField()
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%s(%s)' % (self.product.name, str(self.quantity))

    class Meta:
        abstract = True

class WarehouseTransaction(Stock):
    '''
    product quantity added/removed to/from warehouse
    '''
    stock_type = models.IntegerField(choices=STOCK_TYPES, default=0,
                                     help_text='added or removed from warehouse',
                                     db_index=True)

    @transaction.atomic
    def save(self, *args, **kwargs):
        '''
        kind of app.level trigger when products added/removed to/from stock
        change the counter of warehouse stock if not available create it
        WHY ATOMIC? when in parallel users add or remove items
        this will create inconsistency in the app
        '''
        if not self.quantity:
            # do not save transactions with zero quantity
            return

        super(WarehouseTransaction, self).save(*args, **kwargs)
        try:
            stock = WarehouseStock.objects \
                                .filter(warehouse_id=self.warehouse_id) \
                                .get(product_id=self.product_id)
        except WarehouseStock.DoesNotExist:
            stock = WarehouseStock(product=self.product, warehouse=self.warehouse,
                                   quantity=0)

        quantity = stock.quantity
        if self.stock_type == STOCK_ADD:
            stock.quantity = quantity + self.quantity
        else: # STOCK_DEL
            stock.quantity = quantity - self.quantity
            if stock.quantity < 0:
                raise IntegrityError('Omborda "%s"dan %s ta mavjud, lekin %s ta so\'ralmoqda' % (self.product,quantity, self.quantity))
        stock.save()

class WarehouseStock(Stock):
    '''
    product quantity in warehouse
    same as above difference is this WarehouseStock defines how many products
    are in particular warehouse, WarehouseTransaction defines when
    added or removed item from stock
    NOTE: only WarehouseTransaction can create/update fields in this table
    '''

    def __unicode__(self):
        return '%s(%s)' % (self.product.name, str(self.quantity))

class Warehouse(models.Model):
    company = models.OneToOneField(Company) # for now, only one warehouse for each company
    name = models.CharField(max_length=32)
    stocks = models.ManyToManyField(Product, through='WarehouseStock',
                                      through_fields=('warehouse', 'product'),
                                      related_name='stocks')

    class Meta:
        permissions = (
                ("read_warehouse", "Can read warehouse data"),
        )

    def __unicode__(self):
        return self.name

    def totals(self):
        totals = {}
        for stock in self.warehousestock_set.all():
            totals[stock.product] = stock.quantity
        return totals

    def state(self):
        product_state = []
        available_products = set()
        for stock in self.warehousestock_set.all().select_related('product'):
            product_state.append({'name':stock.product.name,
                                 'quantity':stock.quantity,
                                 'internal':stock.product.internal,
                                 'pk':stock.product.pk})
            available_products.add(stock.product.pk)

        # add not available products as zero value
        for product in Product.objects.filter(~models.Q(pk__in=available_products)):
            product_state.append({'name':product.name,
                                 'quantity':0,
                                 'internal':product.internal,
                                 'pk':product.pk})
        return product_state

    def add_item(self, product, quantity):
        kwargs = {'stock_type':STOCK_ADD,
                 'quantity':quantity,
                 'product':product,
                 'warehouse':self}
        wtx = WarehouseTransaction(**kwargs)
        wtx.save()
        return wtx

    def take_item(self, product, quantity):
        kwargs = {'stock_type':STOCK_DEL,
                 'quantity':quantity,
                 'product':product,
                 'warehouse':self}
        wtx = WarehouseTransaction(**kwargs)
        wtx.save()
        return wtx


@receiver(post_save, sender=Company)
def company_creation_handler(sender, instance, created, **kwargs):
    if not created:
        return

    data = {'company':instance,
            'name':'main_warehouse'}
    wr = Warehouse.objects.create(**data)








