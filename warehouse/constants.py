from django.utils.translation import ugettext_lazy as _

STOCK_ADD = 0
STOCK_DEL = 1

STOCK_TYPES = (
    (STOCK_ADD, _('addition')),    # added to warehouse
    (STOCK_DEL, _('remove')),     # equal to subtraction, or move from one to another warehouse
)
