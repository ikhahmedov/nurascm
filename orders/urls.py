from django.conf.urls import url, patterns
from views import (IncomingOrderView, OutgoingOrderConfirmationAndCreateView,
                   OutgoingOrderCreateView, OutgoingOrderHistoryView,
                   ProcessingBatchView, BatchDetailView,OutgoingOrderUpdateView,
                   OrderDetail, BaseTopUsersListView,BaseTopProductListView,
                   BatchPrintView, IncomingOrderPrintView)


urlpatterns = patterns('',
    url(r'^outgoings/$', OutgoingOrderHistoryView.as_view(), name='outgoing_orders_history'),
    url(r'^outgoings/update/(?P<pk>\d+)/$', OutgoingOrderUpdateView.as_view(), name='outgoing_order_update'),
    url(r'^outgoings/new/$', OutgoingOrderCreateView.as_view(), name='outgoing_orders_new'),
    url(r'^outgoings/new/confirm/$', OutgoingOrderConfirmationAndCreateView.as_view(), name='outgoing_orders_confirmation'),

    url(r'^incomings/new/$', IncomingOrderView.as_view(editable=True), name='incoming_orders_new'),
    url(r'^incomings/processing/$', ProcessingBatchView.as_view(editable=True,processed=False), name='incoming_batch_processing'),
    url(r'^incomings/history/$', ProcessingBatchView.as_view(editable=False,processed=True), name='incoming_batch_history'),
    url(r'^incomings/orderhistory/$', IncomingOrderView.as_view(editable=False), name='incoming_orders_history'),
    url(r'^incomings/batch/(?P<pk>\d+)/$', BatchDetailView.as_view(), name='incoming_batch_details'),
    url(r'^orderdetail/(?P<pk>\d+)/$', OrderDetail.as_view(), name='order_detail'),
    url(r'^statistics/most_frequent/$', BaseTopUsersListView.as_view(order_by_price=False), name='most_frequent'),
    url(r'^statistics/most_sold/$', BaseTopProductListView.as_view(order_by_qty=True), name='most_sold_products'),
    url(r'^incomings/printbatch/(?P<pk>\d+)/$', BatchPrintView.as_view(), name='incoming_batch_print'),
    url(r'^incoming_orders/print/$', IncomingOrderPrintView.as_view(), name='incoming_order_print'),
)
