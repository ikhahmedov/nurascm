from django.db import models, transaction
from companies.models import Company
from warehouse.models import Product, WarehouseTransaction, STOCK_DEL, STOCK_ADD
from orders.constants import *
from django.utils import timezone
from itertools import starmap
# Create your models here.

class OrderProductDetail(models.Model):
    product = models.ForeignKey(Product)
    order   = models.ForeignKey('Order')
    quantity = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return '%s(%s)' % (self.product.name, self.quantity)

class OrderManager(models.Manager):

    def get_queryset(self, *args, **kwargs):
        queryset = super(OrderManager, self).get_queryset(*args, **kwargs)
        return queryset.select_related('requester')

    def outgoings(self, company):
        '''
        outgoing orders
        '''
        return self.get_queryset() \
                .filter(requester=company)

    def incomings(self, company):
        '''
        incoming orders
        '''
        return self.get_queryset() \
                .filter(requested=company)

    @transaction.atomic
    def create_order(self, requester, requested, products_count):
        order = Order(requester=requester, requested=requested,
                      state=ORDER_STATE_NEW)
        order.save()
        for product,quantity in products_count.items():
            detail = OrderProductDetail(
                                product=product,
                                order=order,
                                quantity=quantity)
            detail.save()
        return order

    def update_states(self, order_ids, state):
        return self.get_queryset() \
                    .filter(pk__in=order_ids) \
                    .update(state=state, updated=timezone.now())


class Order(models.Model):
    # requester can be any person not only company
    requester = models.ForeignKey(Company, related_name='requester',blank=True, null=True)
    requested = models.ForeignKey(Company, related_name='requested')
    products  = models.ManyToManyField(Product, through='OrderProductDetail',
                                      through_fields=('order', 'product'))
    state     = models.IntegerField(choices=ORDER_STATES, default=0)
    created   = models.DateTimeField(auto_now_add=True)
    updated   = models.DateTimeField(auto_now=True)
    slug      = models.SlugField(default='')

    objects = OrderManager()

    class Meta:
        permissions = (
                ("create_outgoingorder", "Can create outgoing orders"),
                ("read_outgoingorder", "Can read outgoing orders"),
                ("read_incomingorder", "Can read incoming orders"),
        )

    def save(self):
        # if object is not created yet
        make_slug = False
        if not self.pk:
            make_slug = True
        super(Order, self).save()
        if make_slug:
            self.slug = '%08d' % (self.pk)
            super(Order, self).save()

    def details(self):
        return self.orderproductdetail_set.select_related('product').all()

    def __unicode__(self):
        return '%s' % (self.requester.name)

class OrderBatchManager(models.Manager):
    @transaction.atomic
    def create_batch(self, order_ids):
        batch = OrderBatch(processed=False)
        batch.save()
        batch.orders.add(*order_ids)
        batch.set_state(ORDER_STATE_PROCESSING)
        batch.save()
        return batch

class OrderBatch(models.Model):
    '''
    table where multiple Orders will be processed at once
    '''
    orders  = models.ManyToManyField(Order)
    created = models.DateTimeField(auto_now_add=True)
    processed = models.BooleanField(default=False,
                                    help_text='helps to determine in history or not')
    waiting = models.BooleanField(default=False,
                                  help_text='when orders sent back to clients for acceptance this will be set to True')

    objects = OrderBatchManager()

    def __unicode__(self):
        return '%s' % (self.pk)

    def can_submit(self, company):
        totals = self.product_totals()
        warehouse = company.warehouse.totals()
        errors = []
        for product, qty in totals.items():
            wquantity = warehouse.get(product, None)
            if not wquantity:
                errors.append( 'No %s products available in warehouse' % (product.name) )
            elif wquantity < qty:
                errors.append( 'Not enough %s, available only %s' % (product.name, wquantity) )
        return errors

    def product_totals(self):
        '''
        total number of products in one batch
        '''
        product_mp = {}
        product_prefetch = models.Prefetch('orderproductdetail_set',
                                    queryset=OrderProductDetail \
                                        .objects \
                                        .select_related('product') \
                                        .all())
        for order in self.orders.prefetch_related(product_prefetch).all():
            if order.state == ORDER_STATE_CANCELED:
                # do not count canceled orders
                continue
            for product_order in order.orderproductdetail_set.all():
                product = product_order.product # less query when used '_id'
                quantity = product_order.quantity
                product_mp[product] = product_mp.get(product, 0) + quantity

        return product_mp

    @transaction.atomic
    def set_state(self, state):
        '''
        set state of all orders into given `state`
        NOTE: backwards is not possible for orders,
        after accepted, cannot be canceled
        after cancel cannot be moved to processing and etc,.
        ONLY 2 STATES considered:
        - ORDER_STATE_PROCESSING
        - ORDER_STATE_WAITING
        '''
        if state not in (ORDER_STATE_PROCESSING, ORDER_STATE_WAITING,):
            return False

        if state == ORDER_STATE_WAITING:
            self.waiting = True
            self.save()

        self.orders.filter(state__lte=state).update(state=state, updated=timezone.now())
        return True

    def responds(self):
        '''
        get how many clients requested to their order
        E.g. 4/12 -> 4 of 12 clients accepted or canceled in current batch
        returns tuple (respondeds, total)
        '''
        total = self.orders.all().count()
        responded = self.orders.all() \
                    .filter(state__gte=ORDER_STATE_CANCELED) \
                    .count()
        #return (1,1)
        return (responded, total)

    @transaction.atomic
    def accept(self, warehouse):
        '''
        final decision made for batch, move all items to history
        take products from `warehouse`
        '''
        stats = self.responds()
        if stats[0] != stats[1]:
            # if not all clients responded yet
            # batch should not be closed
            return False
        self.processed = True
        self.save()
        # remove items from warehouse
        # do not worry about item avialability in warehouse
        # triggers in WarehouseTransaction and WarehouseStock will
        # do their job :)
        wts = []

        def recalc(tpl):
            warehouse, product_ids = tpl
            print product_ids, warehouse
            products = {}
            transactions = WarehouseTransaction.objects \
                                .filter(warehouse=warehouse) \
                                .filter(product__in=product_ids) \
                                .values('product', 'stock_type') \
                                .annotate(qty=models.Sum('quantity'))

            take = lambda x,y:{ i['product']:i['qty'] for i in x if i['stock_type'] == y}
            removes = take(transactions, STOCK_DEL)
            adds = take(transactions, STOCK_ADD)
            stocks = {}
            for product in product_ids:
                stocks[product] = adds.get(product,0) - removes.get(product,0)

            for stock in warehouse.warehousestock_set.filter(product__in=product_ids):
                stock.quantity = stocks[stock.product_id]
                stock.save()
            return True

        products = self.product_totals()
        warehouses = {}
        pd_ids = set()
        for product, quantity in products.items():
            if not quantity:
                continue

            wts.append(
                    WarehouseTransaction(
                        product=product,
                        warehouse=warehouse,
                        quantity=quantity,
                        stock_type=STOCK_DEL
                    )
            )
            pd_ids.add(product.id)

        warehouses[warehouse] = pd_ids

        for order in self.orders.filter(state=ORDER_STATE_ACCEPTED):
            warehouse = order.requester.warehouse
            pd_ids = set()
            for detail in order.orderproductdetail_set.all():
                if not detail.quantity:
                    continue
                wts.append(
                        WarehouseTransaction(
                            product_id=detail.product_id,
                            warehouse=warehouse,
                            quantity=detail.quantity,
                            stock_type=STOCK_ADD
                        )
                )
                pd_ids.add(detail.product_id)
            warehouses[warehouse] = warehouses.get(warehouse, set()) | pd_ids

        WarehouseTransaction.objects.bulk_create(wts)
        map(recalc, warehouses.items())
        self.orders \
            .filter(state=ORDER_STATE_ACCEPTED) \
            .update(state=ORDER_STATE_SHIPPED, updated=timezone.now())









