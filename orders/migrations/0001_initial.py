# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0001_initial'),
        ('warehouse', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('state', models.IntegerField(default=0, choices=[(0, 'new'), (1, 'processing'), (2, 'awaiting'), (3, 'canceled'), (4, 'accepted'), (5, 'shipped')])),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('slug', models.SlugField(default=b'')),
            ],
            options={
                'permissions': (('create_outgoingorder', 'Can create outgoing orders'), ('read_outgoingorder', 'Can read outgoing orders'), ('read_incomingorder', 'Can read incoming orders')),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrderBatch',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('processed', models.BooleanField(default=False, help_text=b'helps to determine in history or not')),
                ('waiting', models.BooleanField(default=False, help_text=b'when orders sent back to clients for acceptance this will be set to True')),
                ('orders', models.ManyToManyField(to='orders.Order')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrderProductDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.IntegerField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('order', models.ForeignKey(to='orders.Order')),
                ('product', models.ForeignKey(to='warehouse.Product')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='order',
            name='products',
            field=models.ManyToManyField(to='warehouse.Product', through='orders.OrderProductDetail'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='requested',
            field=models.ForeignKey(related_name='requested', to='companies.Company'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='requester',
            field=models.ForeignKey(related_name='requester', blank=True, to='companies.Company', null=True),
            preserve_default=True,
        ),
    ]
