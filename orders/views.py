from itertools import izip
from datetime import datetime, timedelta
from django.utils import timezone
from django.shortcuts import render
from django.db.utils import IntegrityError
from django.db.models import Prefetch
from django.db import transaction, connection
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy, reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import TemplateView, View, ListView, DetailView
from django.views.generic.detail import ContextMixin
from django.template.response import TemplateResponse
from django import forms
from core.views import BaseSupplyChainViewMixin
from models import Order, OrderBatch, OrderProductDetail
from companies.models import Company, get_main_company
from warehouse.models import Product, Category
from constants import *


PRODUCT_FORM_START = 'product_'

def get_product_counts(request_post):
    size = len(PRODUCT_FORM_START)

    product_counts = {}
    for k,v in request_post.items():
        if k.startswith(PRODUCT_FORM_START) and len(v) > 0 and int(v) > 0:
            product_counts[int(k[size:])] = int(v)
    return product_counts

def query_to_dicts(query_string, *query_args):
    with connection.cursor() as cursor:
        cursor.execute(query_string, query_args)
        col_names = [desc[0] for desc in cursor.description]
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            row_dict = dict(izip(col_names, row))
            yield row_dict
    return

def parse_daterange(daterange):
    tz = timezone.get_default_timezone()
    if daterange is None or len(daterange) is 0:
        today = datetime.now().replace(tzinfo=tz,hour=00,minute=00)
        end = today.replace(hour=23,minute=59)
        start = today - timedelta(days=30)
        #today_end = today.replace(hour=23,minute=59)
        return start,end

    start, end = daterange.split('-')
    start_dt = datetime.strptime(start.strip(), '%m/%d/%Y').replace(tzinfo=tz)
    end_dt = datetime.strptime(end.strip(), '%m/%d/%Y') \
                        .replace(tzinfo=tz,hour=23,minute=59)
    return start_dt, end_dt

# Create your views here.
class OutgoingOrderHistoryView(BaseSupplyChainViewMixin, ListView):
    model = Order
    template_name = 'orders/outgoing_orders_history.html'
    paginate_by = 10

    def get_queryset(self):
        queryset = super(OutgoingOrderHistoryView, self) \
                        .get_queryset() \
                        .filter(requester=self.request.user.company) \
                        .prefetch_related('orderproductdetail_set') \
                        .order_by('-created')
        return queryset

class OutgoingOrderUpdateView(BaseSupplyChainViewMixin, View):
    success_url = reverse_lazy('outgoing_orders_history')

    def post(self, *args, **kwargs):
        POST = self.request.POST
        pk = kwargs['pk']
        order = Order.objects.get(pk=pk)
        if POST.has_key('ok'):
            order.state = ORDER_STATE_ACCEPTED
        else:
            order.state = ORDER_STATE_CANCELED
        order.save()
        return HttpResponseRedirect(self.success_url)


class OutgoingOrderCreateView(BaseSupplyChainViewMixin, TemplateView):
    template_name = 'orders/outgoing_order_new.html'
    post_template_name = 'orders/outgoing_order_confirm.html'

    def get_context_data(self, **kwargs):
        context = super(OutgoingOrderCreateView, self).get_context_data(**kwargs)
        by_category = []

        for category in Category.objects.all().prefetch_related('product_set'):
            mp = []
            for p in category.product_set.filter(internal=False):
                mp.append(p)
            if len(mp):
                by_category.append( (category, mp) )

        context['products'] = by_category
        return context

    def post(self, *args, **kwargs):
        product_counts = get_product_counts(self.request.POST)
        selected = Product.objects.filter(pk__in=product_counts.keys())
        for product in selected:
            setattr(product, 'count', product_counts[product.id])
        # getting navigation
        context = super(OutgoingOrderCreateView, self).get_context_data(**kwargs)
        context['products'] = selected
        return TemplateResponse(request=self.request,
                                template=self.post_template_name,
                                context=context)

class OutgoingOrderConfirmationAndCreateView(BaseSupplyChainViewMixin, View):
    success_url = reverse_lazy('outgoing_orders_history')

    def post(self, *args, **kwargs):
        size = len(PRODUCT_FORM_START)

        product_counts = get_product_counts(self.request.POST)

        requester = self.request.user.company
        requested = get_main_company()
        if len(product_counts):
            products = Product.objects.filter(pk__in=product_counts.keys())
            quantities = {}
            for product in products:
                quantities[product] = product_counts[product.id]
            # create order
            Order.objects.create_order(requester=requester, requested=requested,
                                       products_count=quantities)

        return HttpResponseRedirect(self.success_url)


class IncomingOrderView(BaseSupplyChainViewMixin, ListView):
    editable=True
    template_name = 'orders/incoming_orders.html'
    batch_creation_url = reverse_lazy('incoming_batch_processing')
    model = Order
    paginate_by = 10

    def get_paginate_by(self, queryset):
        if self.editable:
            return None
        return self.paginate_by

    def filter_by_args(self, queryset):
        if self.request.method != 'GET':
            return queryset
        GET = self.request.GET
        company = GET.get('firma', None)
        compid = GET.get('compid', None)
        tel = GET.get('tel', None)
        manzil = GET.get('manzil', None)
        state = GET.get('state', None)
        daterange = GET.get('daterange', None)
        if company:
            queryset = queryset.filter(requester__name__icontains=company)
        if compid:
            queryset = queryset.filter(requester__id=compid)
        if tel:
            queryset = queryset.filter(requester__telephone__icontains=tel)
        if state:
            queryset = queryset.filter(state=state)
        if manzil:
            queryset = queryset.filter(requester__address__icontains=manzil)
        if daterange:
            # daterange format : 03/18/2013 - 03/23/2013
            start_dt, end_dt = parse_daterange(daterange)
            queryset = queryset.filter(created__gte=start_dt, created__lte=end_dt)
        return queryset

    def get_queryset(self):
        user = self.request.user
        queryset = super(IncomingOrderView, self).get_queryset().order_by('-created')
        queryset = queryset.filter(requested=user.company) \
                           .select_related('requester') \
                           .prefetch_related('orderproductdetail_set')
        if self.editable:
            # we are looking at new items
            queryset = queryset.filter(state=ORDER_STATE_NEW)
        queryset = self.filter_by_args(queryset)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(IncomingOrderView, self).get_context_data(**kwargs)
        context['editable'] = self.editable
        return context

    def post(self, *args, **kwargs):
        POST = self.request.POST
        if self.editable and POST.has_key('order_selection'):
            orders = [int(order) for order in POST.getlist('order_selection',[])]
            if POST.has_key('process'):
                OrderBatch.objects.create_batch(orders)
                return HttpResponseRedirect(self.batch_creation_url)
            else:
                Order.objects.update_states(orders, ORDER_STATE_CANCELED)
        return self.get(*args, **kwargs)

class IncomingOrderPrintView(IncomingOrderView):
    paginate_by = 100000
    template_name = 'orders/incoming_orders_print.html'
    editable = False

    def get_context_data(self, **kwargs):
        context = super(IncomingOrderPrintView, self).get_context_data(**kwargs)
        queryset = kwargs.pop('object_list', self.object_list)
        queryset = queryset.order_by('created')

        product_queryset = Product.objects.filter(internal=False).order_by('pk')
        context['products'] = product_queryset
        product_ids = map(lambda x:x.pk, product_queryset)
        product_ids = sorted(product_ids, key=lambda x:x)
        order_details = []

        for order in queryset:
            order_info = [] # data to be printed in sequential way
            order_info.append(order.created)
            ordered_products = {} # contains {product_id : quantity}
            for detail in order.orderproductdetail_set.all().order_by('product__pk'):
                ordered_products[detail.product_id] = detail.quantity
            for p in product_ids:
                qty = ordered_products.get(p, None)
                if qty:
                    order_info.append(qty)
                else:
                    order_info.append(None)
            order_details.append(order_info)

        context['order_details'] = order_details

        return context

class BaseBatchListView(BaseSupplyChainViewMixin, ListView):
    template_name = 'orders/processing_batches.html'
    model = OrderBatch
    editable = True
    processed = False
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(BaseBatchListView, self).get_context_data(**kwargs)
        context['editable'] = self.editable
        return context

    def get_queryset(self, *args, **kwargs):
        return super(BaseBatchListView, self) \
                    .get_queryset(*args, **kwargs) \
                    .filter(processed=self.processed) \
                    .filter(orders__requested=self.request.user.company) \
                    .distinct() \
                    .order_by('-created')

class ProcessingBatchView(BaseBatchListView):

    def post(self, *args, **kwargs):
        POST = self.request.POST
        request = self.request
        if POST.has_key('process'):
            batch_id = int(POST['process'])
            try:
                batch = OrderBatch.objects.get(pk=batch_id)
                accepted = batch.accept(self.request.user.company.warehouse)
            except IntegrityError as ie:
                messages.error(request, ie.message)
            except Exception as e:
                messages.error(request, e.message)
        else:
            pass
        return self.get(request, *args, **kwargs)

class BatchPrintView(BaseSupplyChainViewMixin, DetailView):
    model = OrderBatch
    template_name = 'orders/batch_print.html'

class BatchDetailView(BaseSupplyChainViewMixin, DetailView):
    template_name = 'orders/batch_details.html'
    model = OrderBatch
    prefix = 'qty__'

    def get_context_data(self, **kwargs):
        context = super(BatchDetailView, self).get_context_data(**kwargs)
        context['prefix'] = self.prefix
        context['orders'] = self.object.orders.all()
        return context

    def get_queryset(self, *args, **kwargs):
        queryset = super(BatchDetailView, self).get_queryset(*args, **kwargs)
        product_prefetch = Prefetch('orderproductdetail_set',
                                    queryset=OrderProductDetail.objects.select_related('product').all())
        prefetch = Prefetch('orders',
                            queryset=Order.objects \
                                .prefetch_related(product_prefetch) \
                                .all())
        queryset = queryset.prefetch_related(prefetch)
        return queryset

    @transaction.atomic
    def post(self, *args, **kwargs):
        #
        POST = self.request.POST
        pk = self.kwargs['pk']
        batch = OrderBatch.objects.get(pk=pk)
        accept = POST.has_key('waiting_response')
        save = POST.has_key('save') or accept
        if save:
            # updating values of order quantity
            # get changed only products
            # then update orders
            orders = {}
            for k,v in self.request.POST.iterlists():
                if k.startswith(self.prefix):
                    order, product = k.split('__')[1:]
                    quantity = v
                    if quantity[0] == quantity[1]:
                        # seems not changed
                        continue
                    order_id = int(order)
                    orders.setdefault(order_id, [])
                    orders[order_id].append((product, quantity))
            if orders:
                for order in batch.orders.filter(pk__in=orders.keys()):
                    products = { int(pk):(int(qty[0]), int(qty[1])) for pk, qty in orders[order.pk]}
                    for order_detail in order \
                                         .orderproductdetail_set \
                                         .filter(product__in=products.keys()):
                        values = products[order_detail.product_id]
                        order_detail.quantity = values[0] \
                                            if values[0] != order_detail.quantity \
                                            else values[1]
                        order_detail.save()

        if accept:
            errors = batch.can_submit(self.request.user.company)
            if not errors:
                batch.set_state(ORDER_STATE_WAITING)
            else:
                for error in errors:
                    messages.error(self.request, error)
        return self.get(self.request, *args, **kwargs)


class OrderDetail(BaseSupplyChainViewMixin, DetailView):
    model = Order
    template_name = 'orders/order_detail.html'

    def get_context_data(self, **kwargs):
        context = super(OrderDetail, self).get_context_data(**kwargs)
        context['order_details'] = self.object.orderproductdetail_set.select_related('product').all()
        return context


class BaseTopUsersListView(BaseSupplyChainViewMixin, TemplateView):
    template_name = 'orders/most_clients.html'
    model = Company
    order_by_price = True

    sql_by_price =  \
            " select client.id, client.name, sum(prod.price*odet.quantity) as measurement " \
            " from orders_order ord " \
            " join companies_company client on client.id = ord.requester_id " \
            " join orders_orderproductdetail odet on odet.order_id = ord.id " \
            " join warehouse_product prod on prod.id = odet.product_id " \
            " where ord.created between %s and %s and ord.state = %s" \
            " group by client.id " \
            " order by sum(prod.price*odet.quantity) desc "

    sql_by_frequency = \
            "select client.id, client.name, count(1) as measurement "\
            "from companies_company client "\
            "join orders_order ord on client.id = ord.requester_id "\
            "where ord.created between %s and %s and ord.state = %s " \
            "group by client.id "\
            "order by count(1) desc "

    def get_context_data(self, **kwargs):
        if self.request.method != 'GET':
            return None
        GET = self.request.GET
        daterange = GET.get('daterange', None)

        context = super(BaseTopUsersListView, self).get_context_data(**kwargs)
        start_dt, end_dt = parse_daterange(daterange)
        if self.order_by_price:
            context['measurement_type'] = 'Xarid imkoniyati'
            context['clients'] = query_to_dicts(self.sql_by_price,
                                                *(start_dt, end_dt, ORDER_STATE_SHIPPED))
        else:
            context['measurement_type'] = 'Xarid chastotasi'
            context['clients'] = query_to_dicts(self.sql_by_frequency,
                                                *(start_dt, end_dt, ORDER_STATE_SHIPPED))
        return context


class BaseTopProductListView(BaseSupplyChainViewMixin, TemplateView):
    template_name = 'orders/most_products.html'
    model = Product
    order_by_qty = True

    sql_by_qty =  \
            " select prod.id, prod.name, sum(odet.quantity) as measurement " \
            " from warehouse_product prod " \
            " join orders_orderproductdetail odet on odet.product_id = prod.id " \
            " join orders_order ord on ord.id = odet.order_id " \
            " where ord.created between %s and %s and ord.state = %s" \
            " group by prod.id " \
            " order by sum(odet.quantity) desc "

    sql_by_totalprice = \
            "select prod.id, prod.name, sum(prod.price*odet.quantity) as measurement "\
            "from warehouse_product prod "\
            "join orders_orderproductdetail odet on odet.product_id = prod.id "\
            "join orders_order ord on ord.id = odet.order_id "\
            "where ord.created between %s and %s and ord.state = %s " \
            "group by prod.id "\
            "order by sum(prod.price*odet.quantity) desc "

    def get_context_data(self, **kwargs):
        if self.request.method != 'GET':
            return None
        GET = self.request.GET
        daterange = GET.get('daterange', None)

        context = super(BaseTopProductListView, self).get_context_data(**kwargs)
        start_dt, end_dt = parse_daterange(daterange)
        if self.order_by_qty:
            context['measurement_type'] = 'Miqdor'
            context['products'] = query_to_dicts(self.sql_by_qty,
                                                 *(start_dt, end_dt, ORDER_STATE_SHIPPED))
        else:
            context['measurement_type'] = 'Narx'
            context['products'] = query_to_dicts(self.sql_by_totalprice,
                                                 *(start_dt, end_dt, ORDER_STATE_SHIPPED))
        return context
