from django.utils.translation import ugettext_lazy as _

ORDER_STATE_NEW         = 0  # created by client
ORDER_STATE_PROCESSING  = 1  # modified or kept as it is by admin and moved to batch
ORDER_STATE_WAITING     = 2  # waiting response from client for acceptance/cancelation
ORDER_STATE_CANCELED    = 3
ORDER_STATE_ACCEPTED    = 4
ORDER_STATE_SHIPPED     = 5

ORDER_STATES = (
    (ORDER_STATE_NEW,           _('new')),
    (ORDER_STATE_PROCESSING,    _('processing')),
    (ORDER_STATE_WAITING,       _('awaiting')),
    (ORDER_STATE_CANCELED,      _('canceled')),
    (ORDER_STATE_ACCEPTED,      _('accepted')),
    (ORDER_STATE_SHIPPED,       _('shipped')),
)
