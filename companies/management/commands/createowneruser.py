from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import Permission
from django.utils.encoding import force_str
from companies.models import User, Company
from main.permissions import OWNER_PERMISSIONS
import getpass

class Command(BaseCommand):
    args = ''
    help = 'creates permissions for organization groups'

    def handle(self, *args, **kwargs):
        try:
            owneruser = User.objects.filter(is_comp_owner=True)[0]
            raise CommandError("user available.")
        except:
            pass
        company_name = raw_input(force_str('company name: '))
        company = Company.objects.create(name=company_name)

        username = raw_input(force_str('username: '))
        password = getpass.getpass(force_str('password: '))
        password2 = getpass.getpass(force_str('password(again): '))
        if password != password2:
            raise CommandError("passwords doesn't match.")
        user = User.objects.create_user(username=username,
                                        password=password,
                                        is_comp_owner=True,
                                        company=company)
        perms = Permission.objects.filter(codename__in=OWNER_PERMISSIONS)
        user.user_permissions.add(*perms)









