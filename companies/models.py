from django.db import models
from django.db.utils import IntegrityError
from django.contrib.auth.models import AbstractUser, UserManager, UserManager
from constants import *

# Create your models here.
class Company(models.Model):
    name = models.CharField(max_length=64, verbose_name='Firma nomi')
    address = models.CharField(max_length=64, blank=True, null=True, verbose_name='Manzil')
    telephone = models.CharField(max_length=64, blank=True, null=True,
                                 verbose_name='Telefon', help_text='vergul bilan yozilsin')
    client_type = models.IntegerField(choices=CLIENT_TYPES, default=CLIENT_DILER,
                                 verbose_name='Klient turi')

    class Meta:
        permissions = (
                ("read_company", "Can read company list"),
        )

    def __unicode__(self):
        return '%s' % (self.name)

class CustomUserManager(UserManager):
    def get_queryset(self, *args, **kwargs):
        queryset = super(CustomUserManager, self).get_queryset(*args, **kwargs)
        queryset = queryset.select_related('company')
        return queryset

class User(AbstractUser):
    company = models.OneToOneField(Company, blank=True, null=True)
    is_comp_owner = models.BooleanField(default=False)

    # def has_perm(self, perm, obj=None):
    #     res = super(User, self).has_perm(perm, obj)
    #     print res, perm, self
    #     return res

    def save(self, *args, **kwargs):
        if self.is_superuser in (None, False) and self.company is None:
            raise IntegrityError('every user must belong to company, except superuser')
        super(User, self).save(*args, **kwargs)

    def __unicode__(self):
        return '%s %s (%s)' % (self.first_name, self.last_name, self.username)

# for some reason we dont have multiple company handling yet
# thats why we need to get hard coded company
# basically first one is MAIN company
def get_main_company():
    main_company = Company.objects.all().order_by('pk')[0]
    return main_company
