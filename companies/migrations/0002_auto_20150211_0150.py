# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='address',
            field=models.CharField(max_length=64, null=True, verbose_name=b'Manzil', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='company',
            name='client_type',
            field=models.IntegerField(default=0, verbose_name=b'Klient turi', choices=[(0, b'Diler'), (1, b'Bozor'), (2, b"Do'kon")]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='company',
            name='name',
            field=models.CharField(max_length=64, verbose_name=b'Firma nomi'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='company',
            name='telephone',
            field=models.CharField(help_text=b'vergul bilan yozilsin', max_length=64, null=True, verbose_name=b'Telefon', blank=True),
            preserve_default=True,
        ),
    ]
