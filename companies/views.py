from django.shortcuts import render
from django.contrib.auth.models import Permission
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic import (TemplateView, ListView, CreateView,
                                UpdateView, DeleteView)
from django.views.generic.detail import SingleObjectMixin
from django.db import transaction
from django.db.utils import IntegrityError
from django import forms
from core.views import BaseSupplyChainViewMixin
from models import User, Company, CLIENT_TYPES

from main.permissions import CLIENT_PERMISSIONS

# Create your views here.
class ClientListView(BaseSupplyChainViewMixin, ListView):
    template_name = 'companies/clients.html'
    # first company is MAIN company
    queryset = Company.objects.all().order_by('pk')[1:]


class ClientForm(forms.ModelForm):
    telephone= forms.CharField(widget=forms.Textarea,label='Telefon',
                               required=False,
                               help_text='vergul bilan yozilsin. Mn:+99890 100-00-00, +99890 100-00-01')
    username = forms.CharField(max_length=16, label='Foydalanuvchi nomi')
    password = forms.CharField(widget=forms.PasswordInput, required=False,
                               label='Parol')

    class Meta:
        model = Company

    def __init__(self, *args, **kwargs):
        if kwargs.get('instance'):
            initial = {}
            initial['username'] = kwargs['instance'].user.username
            kwargs['initial'] = initial
        return super(ClientForm, self).__init__(*args, **kwargs)

    def clean_phone(self):
        '''
        phone numbers could be given by comma, new line, space
        we should save them using new lines
        '''
        telephone = self.cleaned_data.get('telephone', '')
        phones = telephone.replace(',', '\n').split()
        return ',\n'.join(phones)


class ClientCreateView(BaseSupplyChainViewMixin,
                       CreateView):
    template_name = 'companies/client_edit.html'
    form_class = ClientForm
    model = Company
    success_url = reverse_lazy('clients_list')

    def form_valid(self, form):
        username     = form.cleaned_data['username']
        password     = form.cleaned_data.get('password', None)
        company_name = form.cleaned_data['name']
        telephone    = form.cleaned_data['telephone']
        address      = form.cleaned_data['address']
        client_type  = form.cleaned_data['client_type']
        with transaction.atomic():
            comp = Company.objects.create(name=company_name, address=address,
                                   client_type=client_type,
                                   telephone=telephone)
            usr = User.objects.create_user(username=username,
                                           password=password,
                                           company=comp)
            perms = Permission.objects.filter(codename__in=CLIENT_PERMISSIONS)
            usr.user_permissions.add(*perms)
        return HttpResponseRedirect(self.success_url)

    def post(self, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            try:
                return self.form_valid(form)
            except IntegrityError as e:
                form.add_error(field=None, error=e.message)

        return self.form_invalid(form)


class ClientEditView(BaseSupplyChainViewMixin,
                    UpdateView):
    template_name = 'companies/client_edit.html'
    form_class = ClientForm
    model = Company
    success_url = reverse_lazy('clients_list')

    def form_valid(self, form):
        username     = form.cleaned_data['username']
        password     = form.cleaned_data.get('password', None)
        company_name = form.cleaned_data['name']
        telephone    = form.cleaned_data['telephone']
        address      = form.cleaned_data['address']
        client_type  = form.cleaned_data['client_type']
        with transaction.atomic():
            company = self.object
            company.telephone = telephone
            company.name = company_name
            company.address = address
            company.client_type = client_type
            user = company.user
            user.username = username
            if password:
                user.set_password(password)
            user.save()
            company.save()

        return HttpResponseRedirect(self.success_url)


class ClientDeleteView(BaseSupplyChainViewMixin,
                       DeleteView):
    template_name = 'companies/client_delete.html'
    model = Company
    success_url = reverse_lazy('clients_list')




