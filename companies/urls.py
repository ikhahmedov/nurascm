from django.conf.urls import url, patterns
from views import (ClientListView, ClientCreateView,
                ClientEditView, ClientDeleteView)


urlpatterns = patterns('',
    url(r'^clients/$', ClientListView.as_view(), name='clients_list'),
    url(r'^clients/new/$', ClientCreateView.as_view(), name='client_new'),
    url(r'^clients/(?P<pk>\d+)/$', ClientEditView.as_view(), name='client_edit'),
    url(r'^clients/(?P<pk>\d+)/delete/$', ClientDeleteView.as_view(), name='client_delete'),
)
