

CLIENT_PERMISSIONS = [
    'read_outgoingorder',
    'create_outgoingorder',
    'read_warehouse',
    'add_warehousetransaction',
]

OWNER_PERMISSIONS = [
    'read_incomingorder',
    'read_company',
    'add_company',
    'change_company',
    'delete_company',
    'read_warehouse',
    'add_warehousetransaction',
    'read_category',
    'read_product',
]
