from django.conf.urls import url, patterns, include
from django.core.urlresolvers import reverse_lazy
from views import IndexView, login


urlpatterns = patterns('',
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^login/$', login, name="login"),
    url(r'^logout/$', 'django.contrib.auth.views.logout',
          {"next_page" : reverse_lazy('login')}, name="logout"),
)
