from django.views.generic import TemplateView, View
from django.http import HttpResponseRedirect
from core.views import BaseSupplyChainViewMixin
from django.contrib.auth.views import login as system_login
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.sites.shortcuts import get_current_site
from django.template.response import TemplateResponse
from django.core.urlresolvers import reverse
from django.contrib.auth import login as auth_login

# Create your views here.
class IndexView(BaseSupplyChainViewMixin, View):

    def get(self, *args, **kwargs):
        user = self.request.user
        if user and not user.is_anonymous():
            if user.is_comp_owner:
                redirect_to = reverse('incoming_orders_new')
            else:
                redirect_to = reverse('outgoing_orders_history')
        else:
            redirect_to = reverse('login')
        return HttpResponseRedirect(redirect_to)

# just copied login()
# instead of redirecting to given page
# always redirect to some page depending on user
@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, template_name='main/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=AuthenticationForm,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.POST.get(redirect_field_name,
                                   request.GET.get(redirect_field_name, ''))

    if request.method == "POST":
        form = authentication_form(request, data=request.POST)
        if form.is_valid():
            user = form.get_user()
            if user.is_comp_owner:
                redirect_to = reverse('incoming_orders_new')
            else:
                redirect_to = reverse('outgoing_orders_history')

            # Okay, security check complete. Log the user in.
            auth_login(request, user)

            return HttpResponseRedirect(redirect_to)
    else:
        form = authentication_form(request)

    current_site = get_current_site(request)

    context = {
        'form': form,
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)
