1. Goal.
Automatization of supply chain management
Brainstorming:
- client orders,
- admin changes order details,
- client accepts or declines changed order
- admin creates clients: NO NO NO
(superuser, using manage.py, why? if company starts creating clients whole architecture
of app will be hacked!!! ideally companies should register and follow company as client)
- add product categories
- add products
- warehouse details

2. Order creation steps.
2.1 Client choses items from list of products, submits (sends POST)
2.2 Next page will show list of items in cart for modification if necessary or final submission.
Clients order will be moved to 'processing orders' screen
2.3 In company's 'New orders' screen will be created new order for client with state (new)
There are 5 states (new, processing, awaiting response, accepted, canceled)
2.4 Admin selects multiple orders and then puts them into processing state
For selected orders will be created 'order batch' object.
'order batch' will be used further for batch processing orders
Batches are atomic, when you change state of batch, all orders
will change state accordingly.
E.g: Batch#1 (order1('processing'), order2('processing'))
if you change state of batch into 'awaiting response'
all orders in batch will get state 'awaiting response'

State of warehouse depends on batches.
E.g: warehouse has 100apples
Batch#1 requires 40apples
Batch#2 will see 60apples in warehouse

2.5 'Batches'. will be shown 'order batch'es.
Every row consists number and related totals in this batch.
E.g.
batch#1 -> apples 60, oranges 30, 20% (cannot be closed)
batch#2 -> apples 30, mandarin 10, 100%, Close batch (moves all orders to history)

in this screen admin can modify order details
related to whether company can supply amount of products required
by clients or not.
If client requested 100items, but company can only provide 50items,
admin should change field related product, then save
After modifying every order in this manner, admin clicks 'send to users'

Batch cannot be updated from one state to another if total items
exceedes(oshib ketmoq) number of available items in warehouse
E.g. warehouse apples 100, oranges 10
batch#2 apples 90 (ok), oranges 70 (not ok, fill warehouse)

2.6 All orders will be marked as 'awaiting response',
until every user accepts or cancels his own order,
admin cannot add or modify states of orders in batch.
NOTE: only user can accept or cancel his order details

2.7 When all users response to their orders with accept or cancel
batch will be moved to history
when moved to history, totals should be only from orders which are accepted
E.g.
batch#1 has order1(apples 100), order2(apples 150)
client of order2 canceled his order
history:
batch# - apples 100 - 1/2 (1 of 2 accepted)

3. Pages
3.1 Outgoing orders (only clients will see)
3.1.1 Create order (see 2.1)
3.1.2 Processing orders (2.2)
Here client should accept or decline order when company put it
into 'awaiting response' state, 'processing' orders cannot be changed in any way!
order details cannot be changed
3.1.3 Order history

3.2 Incoming orders (only companies will see this)
3.2.1 New orders (2.3, 2.4)
3.2.2 Processing orders or 'orders batch' (2.5, 2.6)
3.2.3 History of batches (2.7)

3.3 Clients
List of clients with add button

3.4 Warehouse
3.4.1 Dashboard.
Show total number of products and provide with form
where user can add new products to warehouse
warehouse counter will be incremented for particular product
addition will be added to supply history
3.4.2 Categories
3.4.3 Products
3.4.4 Supply history.


4. Removed but requested functionalities
4.1 Importing exporting from/to excel
4.2 Warehouse information from clients

# todo:
- when ordered 0 items do not get from warehouse
- remove selectbox from new order
- all products in order should be inside clicking orderID
-------------------------
Tarjima uchun:
- Batch details (batch_details.html)
- Create batch (incoming_orders.html)
- Yangi zakazlar (incoming_orders.html)
- Zakaz (order_detail.html)
- Order Batches (processing_batches.html)








