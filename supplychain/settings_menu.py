TITLE = 'title'
VIEW  = 'view_name'
PERM  = 'permissions'
VISIBLE = 'visible'
CHILDS  = 'childs'

NAVIGATION_MENU = [
    {
        TITLE : 'Zakazlar',
        PERM : ['orders.read_outgoingorder',],
        VISIBLE : True,
        CHILDS : [
            {
               TITLE : 'Zakazlar tarixi',
               VIEW : 'outgoing_orders_history',
               PERM : [],
               VISIBLE : True,
               CHILDS : [
                    {
                       TITLE : 'Outgoing order update',
                       VIEW : 'outgoing_order_update',
                       PERM : [],
                       VISIBLE : False,
                    },
               ]
            },
            {
               TITLE : 'Yangi',
               VIEW : 'outgoing_orders_new',
               PERM : ['orders.create_outgoingorder',],
               VISIBLE : True,
               CHILDS : [
                    {
                       TITLE : 'Order submission', # when created order here we will edit/confirm
                       VIEW : 'outgoing_orders_confirmation',
                       PERM : [],
                       VISIBLE : False,
                    },
                ]
            },
        ],
    },
    # Outgoing orders finish
    {
        TITLE : 'Kiruvchi zakazlar',
        PERM : ['orders.read_incomingorder',],
        VISIBLE : True,
        CHILDS : [
            {
                TITLE : 'Yangi zakazlar',
                VIEW : 'incoming_orders_new',
                PERM : [],
                VISIBLE : True
            },
            {
                TITLE : 'Ko\'rilayotgan batch lar',
                VIEW : 'incoming_batch_processing',
                PERM : [],
                VISIBLE : True,
            },
            {
                TITLE : 'Batch lar tarixi',
                VIEW : 'incoming_batch_history',
                PERM : [],
                VISIBLE : True,
            },
            {
                TITLE : 'Zakazlar tarixi',
                VIEW : 'incoming_orders_history',
                PERM : [],
                VISIBLE : True,
            },
            {
                TITLE : 'Batch details',
                VIEW : 'incoming_batch_details',
                PERM : [],
                VISIBLE : False,
            },
            {
                TITLE : 'Batch print',
                VIEW : 'incoming_batch_print',
                PERM : [],
                VISIBLE : False,
            },
            {
                TITLE : 'Orders print',
                VIEW : 'incoming_order_print',
                PERM : [],
                VISIBLE : False,
            }
        ]
    },
    {
        TITLE : 'Klientlar',
        PERM : ['companies.read_company',],
        VISIBLE : True,
        CHILDS : [
            {
                TITLE: 'Klientlar',
                VIEW : 'clients_list',
                PERM : [],
                VISIBLE : True,
            },
            {
                TITLE: 'Client New',
                VIEW : 'client_new',
                PERM : ['companies.add_company',],
                VISIBLE : False,
            },
            {
                TITLE: 'Client Edit',
                VIEW : 'client_edit',
                PERM : ['companies.change_company',],
                VISIBLE : False,
            },
            {
                TITLE: 'Client Delete',
                VIEW : 'client_delete',
                PERM : ['companies.delete_company',],
                VISIBLE : False,
            },
        ]
    },
    {
        TITLE : 'Ombor',
        PERM : ['warehouse.read_warehouse'],
        VISIBLE : True,
        CHILDS : [
            {
                TITLE: 'Ombor',
                VIEW : 'warehouse_dashboard',
                PERM : [],
                VISIBLE : True,
                CHILDS : [
                    {
                        TITLE: 'Stock transaction',
                        VIEW : 'warehouse_stock_transaction',
                        PERM : ['warehouse.add_warehousetransaction',],
                        VISIBLE : False,
                    },
                ]
            },
            {
                TITLE: 'Ombor tarixi',
                VIEW : 'warehouse_history',
                PERM : [],
                VISIBLE : True,
            },
            {
                TITLE: 'Kategoriyalar',
                VIEW : 'warehouse_categories',
                PERM : ['warehouse.read_category',],
                VISIBLE : True,
                CHILDS : [
                    {
                       TITLE : 'Edit category',
                       VIEW : 'warehouse_category_edit',
                       PERM : [],
                       VISIBLE : False,
                    },
                    {
                       TITLE : 'Create category',
                       VIEW : 'warehouse_category_new',
                       PERM : [],
                       VISIBLE : False,
                    },
                    {
                       TITLE : 'Delete category',
                       VIEW : 'warehouse_category_delete',
                       PERM : [],
                       VISIBLE : False,
                    },
                ]
            },
            {
                TITLE: 'Mahsulotlar',
                VIEW : 'warehouse_products',
                PERM : ['warehouse.read_product',],
                VISIBLE : True,
                CHILDS : [
                    {
                       TITLE : 'Edit products',
                       VIEW : 'warehouse_product_edit',
                       PERM : [],
                       VISIBLE : False,
                    },
                    {
                       TITLE : 'Create product',
                       VIEW : 'warehouse_product_new',
                       PERM : [],
                       VISIBLE : False,
                    },
                    {
                       TITLE : 'Delete product',
                       VIEW : 'warehouse_product_delete',
                       PERM : [],
                       VISIBLE : False,
                    },
                ]
            },
        ]
    },
    {
        TITLE : 'Statistika',
        PERM : ['orders.read_incomingorder',],
        VISIBLE : True,
        CHILDS : [
            {
                TITLE : 'Eng ko\'p xarid qilganlar',
                VIEW : 'most_frequent',
                PERM : [],
                VISIBLE : True,
            },
            {
                TITLE : 'Mahsulot(miqdor bo\'yicha)',
                VIEW : 'most_sold_products',
                PERM : [],
                VISIBLE : True,
            },
        ]
    },
]

URL_PERMISSIONS = {}

def build_permissions(menu_items, perms=[]):
    for item in menu_items:
        view_name = item.get(VIEW, None)
        title = item.get(TITLE)
        childs = item.get(CHILDS, [])
        item_perms = perms + item.get(PERM, [])
        if view_name:
            URL_PERMISSIONS[view_name] = item_perms
        else:
            URL_PERMISSIONS[title] = item_perms
        build_permissions(childs, item_perms)

build_permissions(NAVIGATION_MENU, [])
