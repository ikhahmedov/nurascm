from django.conf.urls import patterns, include, url, RegexURLResolver, RegexURLPattern
from django.contrib import admin
from django.contrib.auth.decorators import permission_required, login_required
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'supplychain.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('main.urls')),
    url(r'^warehouse/', include('warehouse.urls')),
    url(r'^companies/', include('companies.urls')),
    url(r'^orders/', include('orders.urls')),
)

def traverse(patterns):
    for pattern in patterns:
        if isinstance(pattern, RegexURLResolver):
            traverse(pattern.url_patterns)
        elif isinstance(pattern, RegexURLPattern):
            callback = pattern.callback
            perms = settings.URL_PERMISSIONS.get(pattern.name, None)
            if perms and len(perms) > 0:
                pattern._callback = permission_required(perm=perms, raise_exception=True)(callback)
                pattern._callback = login_required(pattern.callback)

traverse(urlpatterns)
#settings.URL_PERMISSIONS

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
